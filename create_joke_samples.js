// Download the helper library from https://www.twilio.com/docs/node/install
const accountSid = 'AC01ee26584ee73460e09fd14c941941c7';
const authToken = 'b03345a52e1ed443bc8198489271b47a';
const client = require('twilio')(accountSid, authToken);

phrases = [
    'Tell me a joke',
    'Tell me a joke',
    'I\'d like to hear a joke',
    'Do you know any good jokes?',
    'Joke',
    'Tell joke',
    'Tell me something funny',
    'Make me laugh',
    'I want to hear a joke',
    'Can I hear a joke?',
    'I like jokes',
    'I\'d like to hear a punny joke'
]

// Replace 'UAXXX...' with your Assistant's unique SID https://www.twilio.com/console/autopilot/list
// Replace 'UDXXX...' with the SID for the task you just created.
phrases.forEach(function(item) {
  sample = client.autopilot.assistants('UA3072c2abffae148a6585a411eef42199')
                           .tasks('tell-a-joke')
                           .samples
                           .create({
                             language: 'en-us',
                             taggedText: item,
                           })
                           .then(sample => console.log(sample.sid))
                           .done();
})
