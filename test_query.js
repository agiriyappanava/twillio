// Download the helper library https://www.twilio.com/docs/libraries/node#install

// Your Account Sid and Auth Token from twilio.com/console
const accountSid = 'AC01ee26584ee73460e09fd14c941941c7';
const authToken = 'b03345a52e1ed443bc8198489271b47a';
const client = require('twilio')(accountSid, authToken);

// Replace 'UAXXX...' with your Assistant's unique SID https://www.twilio.com/console/autopilot/list
query = client.autopilot.assistants('UA3072c2abffae148a6585a411eef42199')
                                 .queries
                                 .create({
                                   language: 'en-US',
                                   query: 'password-reset',
                                 })
                                 .then(query => console.log(query.results.task))
                                 .done();
