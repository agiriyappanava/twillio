// Download the helper library from https://www.twilio.com/docs/node/install
const accountSid = 'AC01ee26584ee73460e09fd14c941941c7';
const authToken = 'b03345a52e1ed443bc8198489271b47a';
const client = require('twilio')(accountSid, authToken);

phrases = [
    'hello',
    'hi',
    'Hello',
    'Hi there'
]

// Replace 'UAXXX...' with your Assistant's unique SID https://www.twilio.com/console/autopilot/list
phrases.forEach(function(item) {
  sample = client.autopilot.assistants('UA3072c2abffae148a6585a411eef42199')
                           .tasks('hello-world')
                           .samples
                           .create({
                             language: 'en-us',
                             taggedText: item,
                           })
                           .then(sample => console.log(sample.sid))
                           .done();
})
