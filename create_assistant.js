// Download the helper library from https://www.twilio.com/docs/node/install
// Your Account Sid and Auth Token from twilio.com/console
// DANGER! This is insecure. See http://twil.io/secure
const accountSid = 'AC01ee26584ee73460e09fd14c941941c7';
const authToken = 'b03345a52e1ed443bc8198489271b47a';
const client = require('twilio')(accountSid, authToken);

client.autopilot.assistants
                .create({
                   friendlyName: 'Quickstart Assistant',
                   uniqueName: 'quickstart-assistant'
                 })
                .then(assistant => console.log(assistant.sid));
